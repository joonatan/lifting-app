// Real data for my training on September
require('dotenv').config()
import * as bcrypt from 'bcrypt'
import * as db from '../src/mongo'
import { User } from '../src/user'

const USERNAME = 'jay'
const PASSWORD = 'not-good'

const data =
    [{ date: "2019-09-29",
        exercises: [
            { name: 'Diamonds', count: [ 10, 15, 12, 12 ] },
            { name: 'Plie', count: [ 20, 40, 80 ] },
            { name: 'Handstand', count: [ 5 ] }
        ]
    },
    { date: "2019-09-28",
            exercises: [
                { name: 'Handstand', count: [ 10 ] },
                { name: 'HSPU', count: [ 3, 3, 3, 4, 5, 5, 3] },
                { name: 'Plie', count: [ 20, 20, 20, 40] }
            ]
    },
    { date: "2019-09-27",
            exercises: [
                { name: 'Handstand', count: [ 5 ] },
                { name: 'Plie', count: [ 20 ] },
                { name: 'Wide squat', count: [ 20, 20, 20, 20 ] },
                { name: 'Split-hold', count: [ 2 ] },
                { name: 'Dips', count: [ 5, 10, 11, 12, 10, 10, 8] }
            ]
    },
    { date: "2019-09-26",
            exercises: [
                { name: 'push-ups', count: [ 18, 14, 6, 14, 19 ] },
                { name: 'HSPU', count: [ 5, 4, 3, 4 ] },
                { name: 'Handstand', count: [ 8 ] }
            ]
    },
    { date: "2019-09-25",
            exercises: [
                { name: 'Scapular dips', count: [ 12, 12 ] },
                { name: 'Dips', count: [ 10, 10, 12, 5, 10, 10, 12, 10, 10, 10 ] },
                { name: 'Push-ups', count: [ 5 ] },
                { name: 'Plie', count: [ 20, 20 ] }
            ]
    },
    { date: "2019-09-24",
            exercises: [
                { name: 'Diamonds', count: [ 12 ] },
                { name: 'Dips', count: [ 12 ] },
                { name: 'Handstand', count: [ 5 ] },
                { name: 'HSPU', count: [ 5 ] }
            ]
    },
    { date: "2019-09-23",
            exercises: [
                { name: 'Scapular dips', count: [ 10, 10 ] },
                { name: 'Dips', count: [ 10 ] },
                { name: 'Pike push-ups', count: [ 15, 10, 12, 8, 8, 8, 10, 8, 9, 7 ] },
                { name: 'Wide squats', count: [ 20 ] },
                { name: 'Plie', count: [ 20, 20, 20, 20, 40 ] }
            ]
    },
    { date: "2019-09-22",
            exercises: [
                { name: 'Scapular dips', count: [ 15 ] },
                { name: 'Dips', count: [ 12, 11, 10, 15, 12, 10, 12, 10, 12 ] },
                { name: 'Diamonds', count: [ 11, 12, 8 ] },
                { name: 'Staggered push-ups', count: [ 6 ] }
            ]
    },
    { date: "2019-09-21",
            exercises: [
                { name: 'Scapular dips', count: [ 15 ] },
                { name: 'Pike push-ups', count: [ 8, 6, 6, 8, 12 ] }
            ]
    },
    { date: "2019-09-20",
            exercises: [
                { name: 'Scapular dips', count: [ 20 ] },
                { name: 'Dips', count: [ 10, 10, 12, 12, 10, 10 ] }
        ]
    },
    { date: "2019-09-18",
            exercises: [
                { name: 'Scapular dips', count: [ 10, 20 ] },
                { name: 'Dips', count: [ 10, 10, 10, 10, 12, 11, 10 ] },
                { name: 'Push-ups', count: [ 16, 7 ] }
            ]
    },
    { date: "2019-09-17",
            exercises: [
                { name: 'Push-ups', count: [ 10, 8 ] },
                { name: 'Pike push-ups', count: [ 12, 6, 2, 4, 6, 6, 7, 7, 8 ] },
                { name: 'Scapular dips', count: [ 10, 12, 10 ] }
            ]
    },
    { date: "2019-09-15",
            exercises: [
                { name: 'Push-ups', count: [ 8 ] },
                { name: 'Scapular dips', count: [ 20 ] }
        ]
    },
    { date: "2019-09-14",
            exercises: [
                { name: 'Push-ups', count: [ 20, 15, 10, 10, 4, 4, 12, 15, 8, 8, 11 ] },
                { name: 'Dips', count: [ 9, 10, 10, 9, 8 ] },
                { name: 'Scapular dips', count: [ 10, 18 ] }
            ]
    }]

const getUser = () => {
    return User.findOne({ username: USERNAME }).then(u => {
        if (!u) {
            const pw = PASSWORD
            const salt = bcrypt.genSaltSync(10)
            bcrypt.hash(pw, salt).then( (hash) => {
                return User.create({ username: USERNAME, passwordHash: hash })
            }).then( u => {
                console.log(`user ${u} created`)
                return u
            }).catch((err) => {
                console.log(err)
                return null
            })
        } else {
            console.error('jay Already created: not running init')
            return u
        }
    })
}

const db_name = (process.env.NODE_ENV === 'test') ?
    process.env.TEST_DATABASE :
    process.env.DEV_DATABASE

const user = process.env.USERNAME || ''
const pwd = process.env.PASSWORD || ''
const url = process.env.MONGODB_URL || ''
const name = db_name || ''

const database = db.connect(user, pwd, url, name)


getUser().then(u => {
    const lifts = data.reduce( (acc :any, x:any) => (
        [ ...acc, ...x.exercises.map(a => (
            { user: u._id, date: x.date, name: a.name, count: a.count }))
        ]
    ), [])

    db.Lift.insertMany(lifts).then(x => {
        console.log(x)
    })
    .catch( e => {
        console.error('Error: ', e)
    })
    .finally( () => {
        console.log('finally')
        // cleanup
        db.disconnect()
    })
})
