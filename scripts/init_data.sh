#!/bin/bash

# These require httpie and httpie-auth-jwt

# This is extremely slow, but it demonstrates how to script and use the command line interface
#   have a look at init_data_multi.sh for a faster method
#   and init_jay.ts for typescript example of the API

# Create user first
# This might fail but w/e for now
http :3003/api/user/register username=felix password=good

resp=`http :3003/api/user/login username=felix password=good | grep token`
str=${resp##{\"token\":\"}
token=${str%%\",\"username*}


url=':3003/api/lifts'
#http --auth-type=jwt --auth=$token :3003/api/lifts?date=2019-09-23
http --auth-type=jwt --auth=$token post $url ex="pike push-up" count=10 date=2019-09-23
http --auth-type=jwt --auth=$token post $url ex="pike push-up" count=8 date=2019-09-23
http --auth-type=jwt --auth=$token post $url ex="pike push-up" count=8 date=2019-09-23
http --auth-type=jwt --auth=$token post $url ex="pike push-up" count=6 date=2019-09-23
http --auth-type=jwt --auth=$token post $url ex="pike push-up" count=8 date=2019-09-23

# TODO add multitudes of these, might do loops too
# For past five or ten days (copy from my done list)
http --auth-type=jwt --auth=$token post $url ex=dip count=10 date=2019-09-24
http --auth-type=jwt --auth=$token post $url ex=dip count=10 date=2019-09-24
http --auth-type=jwt --auth=$token post $url ex=dip count=10 date=2019-09-24
http --auth-type=jwt --auth=$token post $url ex=dip count=10 date=2019-09-24
http --auth-type=jwt --auth=$token post $url ex=dip count=10 date=2019-09-24
http --auth-type=jwt --auth=$token post $url ex=dip count=10 date=2019-09-24
http --auth-type=jwt --auth=$token post $url ex="diamond push-up" count=12 date=2019-09-24
http --auth-type=jwt --auth=$token post $url ex="diamond push-up" count=12 date=2019-09-24
http --auth-type=jwt --auth=$token post $url ex="diamond push-up" count=20 date=2019-09-24
http --auth-type=jwt --auth=$token post $url ex="diamond push-up" count=16 date=2019-09-24

http --auth-type=jwt --auth=$token post $url ex=handstand count=30 date=2019-09-25
http --auth-type=jwt --auth=$token post $url ex=handstand count=30 date=2019-09-25
http --auth-type=jwt --auth=$token post $url ex=handstand count=30 date=2019-09-25
http --auth-type=jwt --auth=$token post $url ex=handstand count=30 date=2019-09-25
http --auth-type=jwt --auth=$token post $url ex=handstand count=30 date=2019-09-25

http --auth-type=jwt --auth=$token post $url ex="diamond push-up" count=30 date=2019-09-25
http --auth-type=jwt --auth=$token post $url ex="diamond push-up" count=30 date=2019-09-25
http --auth-type=jwt --auth=$token post $url ex="diamond push-up" count=30 date=2019-09-25
http --auth-type=jwt --auth=$token post $url ex="diamond push-up" count=30 date=2019-09-25
