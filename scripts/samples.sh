#!/bin/bash

resp=`http :3003/api/user/login username=felix password=good | grep token`
str=${resp##{\"token\":\"}
token=${str%%\",\"username*}

http --auth-type=jwt --auth=$token :3003/api/lifts?date=2019-09-25

http --auth-type=jwt --auth=$token :3003/api/lifts?ex=pike

# TODO how to add count and intervals?

# TODO write integration tests using test db and full usage with httpie
