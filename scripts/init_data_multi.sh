#!/bin/bash

# Create user first
# This might fail but w/e for now
http :3003/api/user/register username=felix password=good

resp=`http :3003/api/user/login username=felix password=good | grep token`
str=${resp##{\"token\":\"}
token=${str%%\",\"username*}


url=':3003/api/lifts'
http --auth-type=jwt --auth=$token post $url ex="pike push-up" count:='[10,8,8,6,8]' date=2019-11-23

http --auth-type=jwt --auth=$token post $url ex=dip count:='[10,10,10,10,10,10]' date=2019-11-24
http --auth-type=jwt --auth=$token post $url ex="diamond push-up" count:='[12,12,20,16]' date=2019-11-24

http --auth-type=jwt --auth=$token post $url ex=handstand count:='[30,30,30,30,30]' date=2019-11-25
http --auth-type=jwt --auth=$token post $url ex="diamond push-up" count:='[30,30,30,30]' date=2019-11-25
