/*  Joonatan Kuosa
 *  2019-09-26
 *
 */

import { Schema, Document, Model, model } from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

export declare interface IUser extends Document {
    username: string,
    passwordHash: string
}

export interface UserModel extends Model<IUser> {};

const userSchema = new Schema({
    username: {
        type: String, required: true, unique: true,
        validate: {
            // only word characters, at least 3 of them
            validator: (v :any) => /\w{3}\w*/.test(v)
            //message: (props :any) => `${props.value} not long enough`
        }
    },
    email: { type: String, required: false, unique: true },
    passwordHash: { type: String, required: true },
})

userSchema.set('toJSON', {
    transform: (doc :any, obj :any) => {
        obj.id = obj._id.toString()
        delete obj._id
        delete obj.__v
        delete obj.passwordHash
    }
})
userSchema.plugin(uniqueValidator)

const User = model('User', userSchema)

export { User }
