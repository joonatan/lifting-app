/*  Joonatan Kuosa
 *
 */

import * as jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'
import express from 'express'
import { User } from './user'

const loginRouter = express.Router()

// TODO add delete and me functions

loginRouter.post('/register', (req, res) => {
    console.log('auth: /register')
    const body = req.body

    User.find({ username: body.username }).then(user => {
        if (user.length) {
            res.status(404).json({ error: `username: \'${body.username}\' already exists` })
        } else {
            const pw = body.password
            const salt = bcrypt.genSaltSync(10)
            bcrypt.hash(pw, salt).then( (hash :any) => {
                return User.create({ username: body.username, passwordHash: hash, email: body.email })
            }).then( () => {
                res.status(201).end()
            }).catch((err :any) => {
                res.status(404).json({ error: err })
            })
        }
    })
})

loginRouter.post('/login', (req, res) => {
    const body = req.body
    const username = body.username

    User.findOne({ username: username }).then( (user :any) => {
        const hash = user.passwordHash
        const pw = body.password

        bcrypt.compare(pw, hash).then( (x :any) => {
            if (!x) {
                throw new Error('Incorrect password')
            } else {
                console.log(`succesfull login: ${username}`)
                // TODO this should be a type
                const userForToken = { username: user.username, id: user._id }
                // TODO add error reporting
                const secr = process.env.SECRET || ''
                const token = jwt.sign(userForToken, secr)
                res.status(200).send({ token, username: user.username, name: user.name })
            }
        }).catch((error : any) => {
            console.error(`login error, user: ${username} : ${error}`)
            res.status(401).json({
                where: "loginRouter",
                error: 'invalid username or password'
            })
        })
    }).catch((error) => {
        res.status(401).json({ where: "loginRouter", error: error })
    })
})

export { loginRouter }
