/*  Joonatan Kuosa
 *  2019-09-26
 *
 */

import { Schema, model, Document, Model, connect as mg_connect, connection as mg_connection } from 'mongoose'
import { IUser } from './user'

// TODO add search options
//  - push exercises
//  - partial matches
// totals per day
//
// TODO add a separate exercise type thing
//  pushup - push, triceps
//  dip - push, triceps
//  pullup - pull
//  chinup - pull
//  scapular -> warmup/minor
//  hold -> isometric
//  plie -> quads, legs, push

// TODO make database address (and login) and name configurable

declare interface IExercise extends Document {
    name: string,
    kind: string,
    group: string,
    muscles: string[],
    example: string
}

declare interface ILift extends Document {
    name: string,
    count: number[],
    date: Date,
    notes: string[],
    user: IUser,
    exercise: IExercise
}

export interface LiftModel extends Model<ILift> {};

const liftSchema = new Schema({
    name: { type: String, required: true },
    count: { type: [Number], required: true },
    date: { type: Date, required: false },
    notes: { type: [String], required: false },
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    exercise: { type: Schema.Types.ObjectId, ref: 'Exercise', required: false }
})

liftSchema.set('toJSON', {
    transform: (doc, obj) => {
        obj.id = obj._id.toString()
        delete obj._id
        delete obj.__v
        return obj
    }
})

const Lift = model<ILift>('Lift', liftSchema);

const exerciseSchema = new Schema({
    name: { type: String, required: true },
    kind: { type: String, required: true },     // push, pull, isometric
    group: { type: String, require: true },     // major group of exercises,
    muscles: { type: [String], required: false },
    example: { type: String, required: false }  // url for a video example
})

const Exercise = model('Exercise', exerciseSchema)

const connect = (username : string, password : string, url : string, db_name : string) => {
    const wurl = `mongodb+srv://${username}:${password}@${url}/${db_name}`
    return mg_connect(wurl, { useNewUrlParser: true })
}

const disconnect = () => {
    mg_connection.close()
}

const addLift = ({ name, count, date , user} : any) => {
    return Lift.find({ name, date, user }).then( (res : ILift[]) => {
        if (res.length > 1) {
            console.error('Shouldn\'t happen, more than one copy of an exercise on the same date.')
        }

        if (res.length > 0) {
            res[0].count = Array.isArray(count)
                ? [...res[0].count, ...count]
                : [...res[0].count, count]
            return res[0].save()
        }
        else {
            return Lift.create({
                name: name, count: count, date: date, user: user
            })
        }
    })
}

export {
    Lift,
    addLift,
    connect,
    disconnect,
}
