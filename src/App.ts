/*  Joonatan Kuosa
 *  2019-09-26
 *
 */

require('dotenv').config()
import express from 'express'
import bodyParser from 'body-parser'
import * as jwt from 'jsonwebtoken'

import * as db from './mongo'
import { loginRouter } from './auth'
import { User, IUser } from './user'
import cors from 'cors'
import { Types } from 'mongoose'

declare interface Req extends express.Request {
    user_id: string,
    token: string
}

const tokenExtractor = (request : express.Request, res : express.Response, next : express.NextFunction) => {
    const req = request as Req
    const auth = req.get('authorization')
    if (auth && auth.toLowerCase().startsWith('bearer')) {
        req.token = auth.substring(7)
    }
    next()
}

const req_auth = (request : express.Request, res : express.Response, next : express.NextFunction) => {
    try {
        const req = request as Req
        // TODO add error reporting
        const secr = process.env.SECRET || ''
        const token = jwt.verify(req.token, secr)
        const usr = token as { username : string, id : string }
        req.user_id = usr.id
        next()
    } catch (jwt_err) {
        res.status(400).json({ error: jwt_err })
    }
}


const toDate: (str: string) => Date = (str) => {
    const x = new Date(Date.parse(str))
    return new Date(Date.UTC(x.getFullYear(), x.getMonth(), x.getDate()))
}

// Something super silly because JS doesn't have a date type (only DateTime)
// need UTC conversion otherwise timezone change will move it to the previous day
const today: () => Date = () => {
    const x = new Date()
    console.log(x)
    return new Date(Date.UTC(x.getFullYear(), x.getMonth(), x.getDate()))
}

class Server {
    public app : any
    private db : any

    constructor() {
        this.app = express()
        this.app.use(cors())
        this.app.use(bodyParser.json())
        this.app.use(tokenExtractor)

        const db_name = (process.env.NODE_ENV === 'test') ?
            process.env.TEST_DATABASE :
            process.env.DEV_DATABASE

        // TODO add error reporting
        const user = process.env.USERNAME || ''
        const pwd = process.env.PASSWORD || ''
        const url = process.env.MONGODB_URL || ''
        const name = db_name || ''

        this.db = db.connect(user, pwd, url, name)
        this.mountRoutes()
    }

    private mountRoutes (): void {
        const router = express.Router()
        router.get('/', (req : express.Request, res : express.Response) => {
            res.status(200).end()
        })

        const f_dir = process.env.FRONT_DIR || ''
        if (f_dir !== '') {
            // TODO should check that it exists
            console.log('servince front from: ', f_dir)
            this.app.use(express.static(f_dir))
        }
        else {
            console.log('front not defined, only running backend')
        }

        // User routes
        this.app.use('/api/user', loginRouter)

        // Rest of the methods require Authentication
        this.app.use(req_auth)

        // TODO should we return error if user was not found?
        //  returns 200, but empty list now
        router.get('/api/lifts', (request : express.Request, res : express.Response) => {
            const req = request as Req
            const ex = req.query.ex || ""
            // TODO date should allow wildcarding or partial dates
            // like 2019-09 or 2019-09-*
            const uid = Types.ObjectId(req.user_id)
            const p1 = { name: {$regex: ex} }
            const params = req.query.date
                ? { ...p1, date: new Date(req.query.date) }
                : { ...p1 }
            console.log('GET /api/lifts')
            console.log(params)
            // This works for aggregating by date
            // output should be: [ { date: { ex, [counts], total, user  } } ]
            // I'd like to have the following
            //      - new field for total of counts
            db.Lift.aggregate()
                .match({ ...params, user: uid })
                .group({ _id: '$date', exercises: {
                    $push: { name: "$name", user: "$user", id: "$_id",
                        notes: '$notes', count: '$count' } }
                })
                .sort({ _id: -1 })
                .addFields({ date: "$_id" })
                .project({ _id: 0 })
                .then( (x : any) => {
                    res.json(x)
            }).catch( (e : any) => {
                res.status(400).json(e)
            })
        })

        router.delete('/api/lifts', (request : express.Request, res : express.Response) => {
            const req = request as Req
            // for example
            // '/api/lifts?all'
            // '/api/lifts?date=2019-09-25'
            // We use the all to avoid accidential deletes if it's defined delete all
            // TODO need to add the search parameters
            if (req.query.all !== undefined) {
                console.log('delete lift: ', req.query)
                console.log(req.query.all)
                db.Lift.deleteMany({ user: req.user_id }).then( (x : any) => {
                    console.log('deleted', x)
                    res.status(200).json(x)
                }).catch( (err : any) => {
                    console.log(err)
                    res.status(400).json(err)
                })
            } else {
                res.status(401).json('not supported')
            }
        })

        router.post('/api/lifts', (request : express.Request, res : express.Response) => {
            const req = request as Req
            if (req.body) {
                console.log('post lift: ', req.body)
                User.findById(req.user_id).then( (user : any) => {
                    // Supporting old format
                    const ex : string = req.body.name || req.body.ex
                    const count : number[] = req.body.count
                    const date : string = req.body.date
                    const d : Date = date ? toDate(date) : today()
                    if (d > new Date) {
                        console.error('user posted date is in the future')
                        throw Error('Can\'t post into the future')
                    }
                    else {
                        return db.addLift(
                            { name: ex, count: count, date: d, user: (user as IUser) }
                        )
                    }
                }).then( (x : any) => {
                    res.status(201).json(x)
                }).catch( (e : any) => {
                    console.error('POST lift Error: ', e)
                    if (e instanceof Error) {
                        res.status(400).json({ error: e.message })
                    } else {
                        res.status(400).json({ error: e })
                    }
                })
            } else {
                res.status(400).end()
            }
        })
        this.app.use('/', router)
    }
}

const server = new Server

export default server.app
