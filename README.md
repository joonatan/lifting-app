# Training App

## Idea
- How to track the work you've done when you train irregulary
- No need to login to anything or open a txt file
- Preferably without writing stuff

## Solution
- Cloud backend with datastorage (SQL, noSQL)
- REST API with authentication: post URL pushup 20
- commandline app that does the posting (to inject auth from config)
- web front for posting (I don't need)
- web front for reports
- phone app (PWA with automatic auth)

## Cool stuff
- MongoDB (with Atlas)
- Typescript (React and Node)
- Docker
- Heroku for docker images

## Usage

https://guarded-stream-78473.herokuapp.com/

You can use following login information for testing

user: felix, pw: good

user: jay, pw: not-good

### Command line
Since the main reason was to create an app that can be used from the command line here's a few examples.

Query examples from samples.sh
``` bash
resp=`http :3003/api/user/login username=felix password=good | grep token`
str=${resp##{\"token\":\"}
token=${str%%\",\"username*}

http --auth-type=jwt --auth=$token :3003/api/lifts?date=2019-09-25

http --auth-type=jwt --auth=$token :3003/api/lifts?ex=pike
```

Create a new lift from init_data.sh
``` bash
url=':3003/api/lifts'
http --auth-type=jwt --auth=$token post $url ex="pike push-up" count=10 date=2019-09-23
```

## Installation

### Configuring

Environment configuration (.env file)
``` bash
SECRET=
# Database configuration
MONGODB_URL=
USERNAME=
PASSWORD=
TEST_DATABASE='lifting-test'
DEV_DATABASE='lifting-dev'
```
