module.exports = {
  entry: 'src/index.jsx',
  output: {
    filename: 'bundle.js'
  },
  resolve: {
  // Add .ts and .tsx to extensions
  extensions: [
    '.js',
    '.jsx',
    '.tsx',
    '.ts'
  ]
  },
  module: {
    rules: [
      { test: /\.(js|jsx)$/, use: 'babel-loader', exclude: /node_modules/ },
      { test: /\.ts(x)$/, loader: 'awesome-typescript-loader', exclude: /node_modules/ }
    ]
  }
}
