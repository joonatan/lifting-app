/*  Joonatan Kuosa
 *  2019-08-30
 *
 */
import { useState } from 'react'

const useField = (type : string, val? : string) => {
    const [value, setValue] = useState(val || '')

    const onChange = (event : any) => {
        setValue(event.target.value)
    }

    const reset = (val? : string) => {
        setValue(val || '')
    }

    return { type, value, onChange, reset }
}

export { useField }
