/*  Joonatan Kuosa
 *  2019-09-27
 *
 */
import axios from 'axios'

const loginUrl = '/api/user/login'
const registerUrl = '/api/user/register'
const liftsUrl = '/api/lifts'

export declare interface IUser {
    name: string
    token: string
}

export declare interface ILift {
    date: Date
    name: string
    reps: number[]
}

// Parse the error into a more readable type
const error_handler = (error : any) => {
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
        const err = error.response.data.error || error.response.data
        throw Error(err)
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
    } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
    }
    console.log(error.config);
    throw error
}

const login = (cred : any) : Promise<IUser> => {
    console.log('POST: login')
    const request = axios.post(loginUrl, cred)
    return request
        .then(resp => ({ name: resp.data.username, token: resp.data.token }) )
        .catch( (error : any) => error_handler (error) )
}

const register = (user : any) : Promise<any> => {
    console.log('POST: register')
    return axios.post(registerUrl, user)
        .then(resp => resp)
        .catch( (error : any) => error_handler (error) )
}


const lifts = (user : IUser) : Promise<any> => {
    console.log('GET : lifts : with user = ', user)
    // TODO why doesn't this throw???
    // because if we throw or reject the promise the error hadnling has problems
    // we run into this at the start since the application runs the query
    // at the start without a user.
    // Could fix the issue with checking but I want this to handle
    // no user case gracefully instead.
    if (!user || user.token === '') {
        //throw Error('No user defined, not sending GET')
        //Promise.reject(new Error('No user defined, not sending GET'))
        console.log('No user provided: not sending request')
        return new Promise( () => [] )
    }

    const config = {
        headers: { Authorization: 'bearer '.concat(user.token) }
    }

    const request = axios.get(liftsUrl, config)
    return request.then(response => response.data)
        .catch( (error : any) => error_handler (error) )
}


const post_lift = (user : IUser, lift : ILift) : Promise<ILift> => {
    console.log('POST: lift')
    if (!user) { throw Error('incorrect user') }
    if (!lift) { throw Error('can\'t post empty lift') }
    if (!lift.name) { throw Error('can\'t post empty lift') }

    const config = {
        headers: { Authorization: 'bearer '.concat(user.token) }
    }

    const ex = { name: lift.name, date: lift.date.toString(), count: lift.reps }
    return axios.post(liftsUrl, ex, config)
        .then(res => {
            return { date: res.data.date, name: res.data.name, reps: res.data.count }
        }).catch( (error : any) => error_handler (error) )
}

    /*
const put_blog = (user, blog, params) => {
    if (!user) { throw 'incorrect user' }
    if (!blog) { throw 'can\'t post empty blog' }

    const config = {
        headers: { Authorization: 'bearer '.concat(user.token) }
    }

    const url = host.concat(liftsUrl).concat('/').concat(blog.id)
    const request = axios.put(url, params, config)
    return request.then(response => response.data)
}

const del_blog = (user, blog) => {
    if (!user) { throw 'incorrect user' }
    if (!blog) { throw 'can\'t post empty blog' }

    const config = {
        headers: { Authorization: 'bearer '.concat(user.token) }
    }

    const url = host.concat(liftsUrl).concat('/').concat(blog.id)
    const request = axios.delete(url, config)
    return request.then(response => response.data)
}
*/

export {
    login,
    register,
    lifts,
    post_lift,
}

