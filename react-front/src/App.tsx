/*  Joonatan Kuosa
 *  2019-09-27
 */
import React, { useState, useEffect } from 'react'

import { Container, Button, Tab, Icon, Header } from 'semantic-ui-react'

import NewLift from './components/NewLift'
import Lifts from './components/Lifts'
import Registration from './components/Registration'
import LoginForm from './components/LoginForm'
import Calendar from './components/Cal'
import Flash from './components/Flash'

import * as service from './service'
import { IUser, ILift  } from './service'

import './App.css'

const STORAGE_USER = 'loggedLiftsAppUser'

const App: React.FC = () => {
    const [ user, setUser ] = useState({ name: '', token: '' })
    const [ showRegister, setShowRegister ] = useState(false)
    const [ lifts , setLifts ] = useState([])
    const [ activeIndex, setActiveIndex ] = useState(0)
    const [ flash, setFlashImpl] = useState('')
    const [ errorFlash, setErrorFlashImpl] = useState('')

    // Proxies so we can have timeouts
    const setFlash = (msg : string) => {
        setFlashImpl(msg)
        setTimeout( () => setFlashImpl(''), 3000 )
    }

    const setErrorFlash = (msg : string) => {
        setErrorFlashImpl(msg)
        setTimeout( () => setErrorFlashImpl(''), 3000 )
    }

    const loginHandler = (obj: { username:string,  password:string }): void => {
        service.login(obj).then( (u : IUser) => {
            console.log('login succesful: ', u)
            setFlash('Welcome back ' + u.name)
            setUser(u)
            window.localStorage.setItem( STORAGE_USER, JSON.stringify(u) )
        }).catch(err => {
            console.error('login handler failed: ', err)
            setErrorFlash('Login failed: ' + err)
        })
    }

    const registrationHandler = (obj: { username:string, email:string, password:string }): Promise<any> => {
        return service.register(obj).then( (res : any) => {
            setFlash('Registering succesful: please log in.')
            setShowRegister(false)
            return true
        }).catch(err => {
            console.error('Registration failed: ', err)
            setErrorFlash('Failed to Register, because ' + err.message)
            return false
        })
    }

    const handleTabChange = (e : any, data : any) => {
        setActiveIndex(data.activeIndex)
    }

    const handlePostNewLift = (lift : ILift): Promise<ILift>  => {
        return service.post_lift(user, lift).then( (x : ILift) => {
            // TODO doing a full cache clear, not a good idea in real application
            // should use incremential update, but it creates more problems.
            service.lifts(user).then( (res) => setLifts(res) )
            return x
        })
    }

    const handleLogout = (user :IUser) => {
        setUser({ name: '', token: '' })
        window.localStorage.removeItem(STORAGE_USER)
        setFlash('See you ' + user.name)
    }

    // Login effect
    useEffect( () => {
        const loggedUserJSON = window.localStorage.getItem(STORAGE_USER)
        if (loggedUserJSON) {
            const user = JSON.parse(loggedUserJSON)
            setUser(user)
        }
    }, [])

    // Lifts list
    useEffect( () => {
        service.lifts(user)
            .then( res => setLifts(res) )
            .catch( err => {
                setErrorFlash('GET lifts failed: ' + err)
                console.error('GET lifts failed: ', err)
            })
    }, [user])

    // One tab for calendar, one for the whole list, one for the recent and add new
    //    later we would add statistics, user management, exercises etc.
    // TODO add a short list of this months lifts to Calenddar
    //
    // Fixing mobile menu
    // Too lazy to write custom menu component that has only icons for mobile version
    // so spamming the html with extra elements that are hidden.
    const panes = [
        {
            menuItem: { key: 'calendar-d', icon: 'calendar', content: 'Calendar', className: 'mobile hidden' },
            render: () => <Tab.Pane attached={false} ><Calendar lifts={lifts} /> </Tab.Pane>,
        },
        {
            menuItem: { key: 'calendar-m', icon: 'calendar', content: '', className: 'mobile only' },
            render: () => <Tab.Pane attached={false} ><Calendar lifts={lifts} /> </Tab.Pane>,
        },
        {
            menuItem: { key: 'add-d', icon: 'add', content: 'Add', className: 'mobile hidden' },
            render: () => <Tab.Pane attached={false}> <NewLift post={handlePostNewLift} setError={setErrorFlash} setMsg={setFlash} /> </Tab.Pane>,
        },
        {
            menuItem: { key: 'add-m', icon: 'add', content: '', className: 'mobile only' },
            render: () => <Tab.Pane attached={false}> <NewLift post={handlePostNewLift} setError={setErrorFlash} setMsg={setFlash} /> </Tab.Pane>,
        },
        {
            menuItem: 'All',
            render: () => <Tab.Pane attached={false}> <Lifts lifts={lifts} /> </Tab.Pane>,
        }
    ]


    const Tabs = () => <Tab menu={{ pointing: true }} panes={panes} activeIndex={activeIndex} onTabChange={handleTabChange} />

    // TODO should the Flash be between the Menu and Tabs (need to separate the tabs them)
    return (
        <div>
            <Flash msg={errorFlash} look={'error'} />
            <Flash msg={flash} look={'info'} />
            <Header as='h1' textAlign='center'>Lifts App</Header>
            {user.token === ''
            ?
                <Container textAlign='center'>
                    {showRegister
                        ?
                        <div>
                            <Registration register={registrationHandler} setError={setErrorFlash} setFlash={setFlash} />
                            or <Button primary onClick={() => setShowRegister(!showRegister)} >Login</Button>
                        </div>
                        :
                        <div>
                            <LoginForm login={loginHandler} />
                            or <Button secondary onClick={() => setShowRegister(!showRegister)} >Register</Button>
                        </div>
                    }
                </Container>
            : (
                <Container>
                    <Button floated="right" icon onClick={() => handleLogout(user)} labelPosition='right' >
                        Log out <Icon name='sign out' />
                    </Button>
                    <Tabs />
                </Container>
            )
            }
        </div>
    )
}

export default App
