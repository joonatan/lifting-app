/* Joonatan Kuosa
 * 2019-12-10
 */
import React, { useState, useMemo } from 'react'
//import Calendar from 'react-calendar'
import Calendar from 'react-calendar/dist/entry.nostyle';

import { Icon, Button, Statistic, Container } from 'semantic-ui-react'

import './Calendar.css'

// Colour coding based on stats
// need css for it : trained that day, didn't, still coming
//      disable anything coming in the future
// show on green days trained (later we use a heatmap for it)
const Cal= (props: { lifts : any } ) => {
    const [ date, setDate ] = useState(new Date())

    // TODO can we show multiple months with this widget?
    // TODO can we use the object format for the function or do we need to use the props?
    //
    // TODO month change should reset the date
    // TODO colours should blend when selecting (so we can see red/green)

    const comp = (d1 : Date, d2 : Date) => {
        return d1.getFullYear() === d2.getFullYear()
            && d1.getMonth() === d2.getMonth()
            && d1.getDate() === d2.getDate()
    }

    const handleThisMonth = () => {
        setDate(new Date())
    }

    const handlePrevMonth = () => {
        setDate(new Date(date.getFullYear(), date.getMonth() -1, date.getDate()))
    }

    const handleNextMonth = () => {
        setDate(new Date(date.getFullYear(), date.getMonth() +1, date.getDate()))
    }

    // TODO this shouldn't be run everytime we change the selection (redraw)
    // same as the filter below, we can calculate these easily enough and save them into a state
    const dates = useMemo( () =>
        props.lifts.map((lift : any) => new Date(lift.date)),
        [props.lifts]
    )

    const start_date = useMemo( () =>
        dates.reduce( (acc : Date, x : Date) => x < acc ? x : acc, new Date() ),
        [dates]
    )

    const handleFirstMonth = () => {
        setDate(start_date)
    }

    const isSameMonth = (d1 : Date, d2 : Date) => {
        return d1.getFullYear() === d2.getFullYear() && d1.getMonth() === d2.getMonth()
    }

    const month_count = ((new Date()).getFullYear() - start_date.getFullYear())*12 + ((new Date()).getMonth() - start_date.getMonth() + 1)

    // TODO need test, how the over at 12 works
    const months = [...Array(month_count).keys()].map( (i :number) => {
        const x : number = i % 12 + start_date.getMonth()
        const m : number = x % 12
        const y : number = start_date.getFullYear() + Math.floor(x/12)
        return {
            n: i,
            year: y,
            month: (new Date(y, m, 1)).toLocaleString('default', { month: 'long' })
        }
    })

    const view_month_name = date.toLocaleString('default', { month: 'long' });

    const create_button = (cb : any, label : string, disabled : boolean, icon_name? : 'angle left' | 'angle right', icon_pos? : 'left' | 'right') => {
        return (
            <Button onClick={cb} secondary disabled={disabled} icon labelPosition={icon_pos} >{label}
                {icon_name && <Icon name={icon_name} />}
            </Button>
        )
    }

    return (
        <div className="Calendar">
            {
                // TODO the top button should open a yearly view
                // TODO should remove value (because it adds a confusing cursor,
                //  problem is that removing it breaks the Calendar navigation
            }
            <Button.Group size='massive' attached='top'>
               <Button onClick={handleThisMonth} >{view_month_name}</Button>
            </Button.Group>
            <Calendar
                onChange={(date) => setDate(date as Date)}
                value={date}
                maxDate={new Date()}
                minDate={start_date}
                showNavigation={false}
                tileClassName={(tile) => {
                    return (dates.filter( (x : Date) => comp(x, tile.date) )
                        .length > 0) ? 'trained' : 'not-trained'
                }}
            />
            <Button.Group attached='top'>
                {create_button(handleFirstMonth, 'First', isSameMonth(date, start_date))}
                {create_button(handlePrevMonth, 'Prev', isSameMonth(date, start_date), 'angle left', 'left')}
                {create_button(handleNextMonth, 'Next', isSameMonth(new Date(), date), 'angle right', 'right')}
                {create_button(handleThisMonth, 'Last', isSameMonth(new Date(), date))}
            </Button.Group>
            <br />
            {
                // TODO counters need to be counted: current streak, longest streak, days
                // TODO add per month stastics : need to calculate them
                //
                // TODO text aligment doesn't work like I want it to
                //  The problem is that with mobile view the big numbers aren't aligned with each other.
                //  Instead the length of the description text moves the elements.
                //  Find how to configure a flex grid for this.
            }
            <Container textAlign='center'>
                <Statistic label='Total' value={dates.length} />
                <Statistic label='Current Streak' value='?' />
                <Statistic label='Longest Streak' value='?' />
                <Statistic label='Total Months' value={month_count} />
            </Container>
            <Container textAlign='center'>
                { months.map( (x : { n : number, year : number, month : string }) => {
                    const str :string = (+x.year + ' ' + x.month)
                    return <Statistic key={str} label={str} value='?' />
                  })
                }
            </Container>
            <br />
        </div>
    )
}

export default Cal
