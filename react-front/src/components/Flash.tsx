/*  Joonatan Kuosa
 *  2019-12-11
 *
 */
import React from 'react'
import { Message } from 'semantic-ui-react'

const Flash = (props: { msg : string, look? : 'error' | 'info' }) => (
    !props.msg
        ? null
        : (props.look === 'error')
            ? ( <Message negative>
                <p> {props.msg} </p>
            </Message>
            )
            : ( <Message positive>
                <p> {props.msg} </p>
            </Message>
            )
)

export default Flash
