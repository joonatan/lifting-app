/*  Joonatan Kuosa
 *  2019-12-09
 */
import React from 'react'

import { Form, Segment, Button, Label } from 'semantic-ui-react'

import { useField } from '../hooks'

import { ILift } from '../service'

const NewLift = (props: { post : (obj:ILift) => Promise<ILift>
                        , setError : (msg:string) => void
                        , setMsg : (msg:string) => void
    }) => {
    const today = () : string => {
        const d = new Date()
        return +d.getFullYear() +'-'+ +(d.getMonth()+1) + '-' + d.getDate();
    }

    // TODO this should have autocomplete
    const lift = useField('text')
    const reps = useField('text')
    const date = useField('date', today())

    // Filters out all non numbers
    const parseNumbers = (x? : string): number[] => {
        return (x ? x.split(/\W/).map(a => +a).filter(a => a > 0) : [])
    }

    const handleSubmit = (event : any): void => {
        const r : number[] = parseNumbers(reps.value)
        const l : string = lift.value || ''
        const d : Date = date.value ? new Date(date.value) : new Date()

        // Using Flash here causes a redraw of the full window which removes all the state data
        if (l === '') {
            console.error('Can\'t post empty lift.')
        }
        else if (r.length === 0) {
            console.error('Can\'t post lift without reps.')
            // need a reset to clear non number data
            reps.reset()
        }
        else if (d > new Date()) {
            console.error('Can\'t post lift in the future.')
            date.reset(today())
        }
        else {
            props.post({ name: l, reps: r, date: d })
                .then( (x : ILift) => {
                    console.log('data: ', x)
                    lift.reset()
                    reps.reset()
                    date.reset(today())
                    props.setMsg('Added ' + x.name + ' exercise')
                })
                .catch( (err : any) => {
                    console.error(err)
                    if (err instanceof Error) {
                        props.setError(err.message)
                    } else if (err instanceof String) {
                        props.setError(err as string)
                    } else {
                        props.setError('Error: something really weird.')
                    }
                })
        }
    }

    // TODO should add focus check on the useField onChange handler
    //     at the moment we display the label if the field is not empty
    // TODO should have a guidance for the user if we change things
    //     for example changing date from future to this day during posting
    //     can be implemented either with labels or more likely with tooltips/popups
    return (
    <Form size='large' onSubmit={handleSubmit}>
        <Segment stacked>
            <Form.Field>
                <Form.Input placeholder='Lift' {...lift} reset=''/>
                { lift.value === '' && <Label pointing prompt> Please enter a name</Label> }
            </Form.Field>
            <Form.Field>
                <Form.Input placeholder='Reps' {...reps} reset=''/>
                { parseNumbers(reps.value).length === 0  && <Label pointing prompt> Please enter numbers separated by spaces</Label> }
            </Form.Field>
            <Form.Field>
                <Form.Input fluid placeholder='Date' {...date} reset='' />
                { new Date(date.value) > (new Date()) && <Label pointing prompt> Can't post in the future</Label> }
            </Form.Field>
            <Button size='large' primary type='submit'>Post</Button>
        </Segment>
    </Form>
    )
}

export default NewLift
