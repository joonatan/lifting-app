/*  Joonatan Kuosa
 *  2019-12-13
 */
import React from 'react'

import { Grid, Header, Segment, Form, Button } from 'semantic-ui-react'

import { useField } from '../hooks'

const Registration = (props: {
        register : (obj:any) => void,
        setError : (str:string) => void,
        setFlash : (str:string) => void
    }) => {
    const username = useField('text')
    const email = useField('email')
    const password = useField('password')
    const password_again = useField('password')

    // FIXME Event type
    const handleRegister = (event : any): void => {
        event.preventDefault()

        if (username.value === '') {
            props.setError('Username required')
        } else if (password.value === '') {
            props.setError('Password required')
        } else if (password.value !== password_again.value) {
            props.setError('Passwords didn\'t match. Please try again.')
        } else {
            props.register({ username: username.value, password: password.value, email: email.value })
        }

        password.reset()
        password_again.reset()
    }

    // TODO change the fields and text for this to be Registration
    // TODO send an email to confirm the registration address
    //      add a user toggle for confirmed
    return (
        <Grid textAlign='center'>  { /* style={{ height: '100vh' }}> */ }
            <Grid.Column style={{ maxWidth: 450 }}>
            <Header as='h2' color='teal' textAlign='center'>
            </Header>
            <Form size='large' onSubmit={handleRegister}>
                <Segment stacked>
                    <Form.Input fluid icon='user' iconPosition='left' placeholder='Username'
                        {...username} reset='' />
                    <Form.Input fluid icon='user' iconPosition='left' placeholder='Email'
                        {...email} reset='' />
                    <Form.Input fluid icon='lock' iconPosition='left' placeholder='Password'
                        {...password} reset='' />
                    <Form.Input fluid icon='lock' iconPosition='left' placeholder='Re-type Password'
                        {...password_again} reset='' />
                    <Button fluid size='large' secondary type='submit'>Register</Button>
                </Segment>
            </Form>
        </Grid.Column>
        </Grid>
    )
}

export default Registration
