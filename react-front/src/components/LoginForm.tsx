/*  Joonatan Kuosa
 *  2019-09-27
 */
import React from 'react'

import { Grid, Header, Segment, Form, Button } from 'semantic-ui-react'

import { useField } from '../hooks'

const LoginForm = (props: { login : (obj:any) => void }) => {
    const username = useField('text')
    const password = useField('password')

    // FIXME Event type
    const handleLogin = (event : any): void => {
        event.preventDefault()

        props.login({ username: username.value, password: password.value })

        username.reset()
        password.reset()
    }

    return (
        <Grid textAlign='center'>  { /* style={{ height: '100vh' }}> */ }
            <Grid.Column style={{ maxWidth: 450 }}>
            <Header as='h2' color='teal' textAlign='center'>
            </Header>
            <Form size='large' onSubmit={handleLogin}>
                <Segment stacked>
                    <Form.Input fluid icon='user' iconPosition='left' placeholder='Username'
                        {...username} reset='' />
                    <Form.Input fluid icon='lock' iconPosition='left' placeholder='Password'
                        {...password} reset='' />
                    <Button fluid size='large' primary type='submit'>Login</Button>
                </Segment>
            </Form>
        </Grid.Column>
        </Grid>
    )
}

export default LoginForm
