/*  Joonatan Kuosa
 *  2019-09-27
 */
import React from 'react'

import { Grid } from 'semantic-ui-react'

// FIXME Lift type : make it into an iterface or smth
// { id, name, count, ?notes, ?date, user  }
const Lifts = (props: { lifts: any }) => {

    // TODO Add the day of the week to the human readable date
    const forHumans: (d : string) => string = (d) => {
        const re = /\d{4}-\d{2}-\d{2}/
        const m = d.match(re)
        return m && m.length > 0 ? m[0] : d
    }

    // TODO make notes just a small icon that gives extra info on hoover (if it is set)
    // TODO center the Header (date) in the grid
    const headers = [
            <Grid.Row key='lift-grid-header'>
                <Grid.Column>name</Grid.Column>
                <Grid.Column>reps</Grid.Column>
                <Grid.Column>total</Grid.Column>
            </Grid.Row>
    ]
    const grid = props.lifts.reduce( (acc :any, lift :any) => (
        [ ...acc,
                <Grid.Row key={lift.date}>
                    <Grid.Column>
                        <h4> {forHumans(lift.date)} </h4>
                    </Grid.Column>
                </Grid.Row>,
         ...lift.exercises.map( (x :any) => (
            <Grid.Row key={lift.date + x.name}>
                <Grid.Column>{x.name}</Grid.Column>
                <Grid.Column>{x.count.reduce(
                    (acc :string, xc :any) => acc + ', ' + xc
                )}</Grid.Column>
                <Grid.Column>{x.count.reduce(
                    (acc :string, xc :any) => acc + xc, 0
                )}</Grid.Column>
            </Grid.Row>
         ))
        ]
    ), headers)

    return (
        <div>
            <Grid columns={3} celled>
            {grid}
            </Grid>
        </div>
    )
}

export default Lifts
