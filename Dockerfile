FROM node:12.13.1-alpine as build
WORKDIR /app
RUN apk --no-cache add --virtual builds-deps build-base python

ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
RUN npm install -g ts-node
RUN npm install --silent

# Copy build files
COPY react-front/ /app/react-front
COPY src/ /app/src
COPY .env /app/
COPY tsconfig.json /app/

RUN npm run build --silent

# Make a smaller image for release
FROM node:12.13.1-alpine
WORKDIR /app
COPY --from=build /app/build /app/
COPY --from=build /app/node_modules /app/node_modules
COPY --from=build /app/.env /app/
ENV PATH /app/node_modules/.bin:$PATH

EXPOSE 80

ENV PORT 80
ENV FRONT_DIR /app/front
CMD ["node", "/app/index.js"]
